
#include <ncurses.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>



#define Nw 12
#define Nh 16


/* Called to kill the program */
static void finish(int sig) {
  endwin();
  exit(0);
}

enum {
  shape_T = 0,
  shape_O,
  shape_L,
  shape_J,
  shale_I,
  shape_S,
  shape_Z,
} shape;

unsigned char lines[Nw][Nh];
int score = 0;
short pos_x, pos_y;
unsigned char rot;

short blocks[shape_Z+1][4][2]
  = {{{-1,0},{0,0},{+1,0},{0,-1}},
     {{-1,0},{0,0},{-1,1},{0,+1}},
     {{0,+1},{0,0},{0,-1},{+1,-1}},
     {{0,+1},{0,0},{0,-1},{-1,-1}},
     {{0,+1},{0,0},{0,-1},{0,-2}},
     {{0,+1},{0,0},{+1,0},{+1,-1}},
     {{0,+1},{0,0},{-1,0},{-1,-1}}};

void get_pos(short pos[2], short block) {
  short t;
  pos[0] = blocks[shape][block][0];
  pos[1] = blocks[shape][block][1];
  switch(rot) {
    case 0:
      break;
    case 1:
      t = pos[1];
      pos[1] = -pos[0];
      pos[0] = t;
      break;
    case 2:
      pos[0] = -pos[0];
      pos[1] = -pos[1];
      break;
    case 3:
      t = pos[1];
      pos[1] = pos[0];
      pos[0] = -t;
      break;
    default:
      finish(1);
  }
}


bool check_valid_location() {
  short block;
  for(block=0; block<4; ++block) {
    short pos[2];
    get_pos(pos,block);
    if(pos_x + pos[0] < 0) {
      return false;
    }
    if(pos_x + pos[0] >= Nw) {
      return false;
    }
    if(pos_y + pos[1] < 0) {
      return false;
    }
    if(pos_y + pos[1] >= Nh) {
      return false;
    }
  }
  return true;
}


void force_valid_location() {
  short block;
  for(block=0; block<4; ++block) {
    short pos[2];
    get_pos(pos,block);
    if(pos_x + pos[0] < 0) {
      pos_x ++;
      block = 0;
    }
    if(pos_x + pos[0] >= Nw) {
      pos_x --;
      block = 0;
    }
    if(pos_y + pos[1] < 0) {
      pos_y ++;
      block = 0;
    }
    if(pos_y + pos[1] >= Nh) {
      pos_y --;
      block = 0;
    }
  }
}
 

void check_lines() {
  int num_removed=0,row,col;
  // Start at bottom and work up
  for(row=0; row<Nh; ++row) {
    bool full = true;
    for(col=0; col<Nw; ++col) {
      if(lines[col][row]==0) {
        full = false;
        break;
      }
    }
    if(full) {
      num_removed += 1;
      score += 10*num_removed;
    }
    if(row + num_removed < Nh) {
      for(col=0; col<Nw; ++col) {
        lines[col][row] = lines[col][row+num_removed];
      }
    } else {
      for(col=0; col<Nw; ++col) {
        lines[col][row] = 0;
      }
    }
    if(full) row--;
  }
}


bool check_intersection() {
  int block;
  for(block=0; block<4; ++block) {
    short pos[2];
    get_pos(pos,block);
    if(lines[pos_x+pos[0]][pos_y+pos[1]] > 0) {
      return true;
    }
  }
  return false;
}


void drop() {
  short block;
  bool collided = false;
  // check collision
  pos_y -= 1;

  if(!check_valid_location()) {
    pos_y += 1;
    collided = true;
  }
  if(!collided && check_intersection()) {
    pos_y += 1;
    collided = true;
  }
  if(collided) {
    // calcify!
    for(block=0; block<4; ++block) {
      short pos[2];
      get_pos(pos,block);
      lines[pos_x+pos[0]][pos_y+pos[1]] = 'a' + shape;
    }
    check_lines();
    shape = rand()%(shape_Z+1);
    pos_x = Nw/2;
    pos_y = Nh-2;
    rot = 0;
    force_valid_location();
    if(check_intersection()) {
      endwin();
      if(score==0)
        printf("You have lost, you're meant to complete lines (%d)\n",score);
      else if(score<100)
        printf("You have lost, the game and your honour (%d)\n",score);
      else if(score<1000)
        printf("You have lost, perhaps this game is not for you (%d)\n",score);
      else if(score<10000)
        printf("You have lost, You survived long enough to not be a total embarrasement (%d)\n",score);
      else 
        printf("You have lost, I bet you're not proud with the time that took (%d)\n",score);
      exit(0);
    }
  }
}


void draw() {
  int row,col,block;
  // Draw the blocks already down
  for(row=0; row<Nh; ++row) {
    move(4+Nh - row,4);
    for(col=0; col<Nw; ++col) {
      char c = lines[col][row];
      if(c==0) addch(' ');
      else     addch(c);
    }
  }
  attron(COLOR_PAIR(1));
  for(block=0; block<4; ++block) {
    short pos[2];
    get_pos(pos,block);
    pos[0] += pos_x;
    pos[1] += pos_y;
    mvprintw(4+Nh-pos[1],4+pos[0],"%c", 'a'+shape);
  }
  attroff(COLOR_PAIR(1));
  mvprintw(4+Nh/2,4+Nw+4,"%4d", score);
}


/* main, you know what main does right? */
int main(int argc, char **argv) {
  int i,j;
  struct timespec time,last;
  signal(SIGINT, finish);

  
  /* Initialise ncurses */
  initscr();
  keypad(stdscr, TRUE);
  cbreak();
  noecho();
  start_color();
  init_pair(1,COLOR_RED,COLOR_BLACK);
  timeout(10);
  
  for(i=0;i<Nw; ++i)
  for(j=0;j<Nh; ++j) {
    lines[i][j] = 0;
  }
  pos_x = Nw/2;
  pos_y = Nh+2;
  rot = 0;
  force_valid_location();
  
  // Border
  move(3,4);
  for(i=0; i<Nw; ++i) {
    addch('-');
  }
  move(4+Nh+1,4);
  for(i=0; i<Nw; ++i) {
    addch('-');
  }
  for(i=0; i<Nh+1; ++i) {
    move(4+i,3);
    addch('|');
    move(4+i,3+Nw+1);
    addch('|');
  }

  /* Run program */

  clock_gettime(CLOCK_MONOTONIC,&last);
  while(true) {
    int c;
    bool do_draw = false;
    clock_gettime(CLOCK_MONOTONIC,&time);
    if( (double)(time.tv_sec-last.tv_sec)+
       ((double)(time.tv_nsec) - (double)(last.tv_nsec))/1e9 > 0.5) {
      drop();
      do_draw = true;
      last = time;
    }
    c = getch();
    switch(c) {
      case KEY_UP:
        rot = (rot + 1)%4;
        if(!check_valid_location()) {
          rot = (rot + 3)%4;
        }
        do_draw = true;
        break;
      case KEY_LEFT:
        pos_x -= 1;
        if(!check_valid_location() || check_intersection()) {
          pos_x += 1;
        }
        do_draw = true;
        break;
      case KEY_RIGHT:
        pos_x += 1;
        if(!check_valid_location() || check_intersection()) {
          pos_x -= 1;
        }
        do_draw = true;
        break;
      case KEY_DOWN:
        drop();
        do_draw = true;
        break;
      default:
        break;
    }
    if(do_draw) {
      draw();
    }
  }
  return 0;
}





