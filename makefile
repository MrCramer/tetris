
CFLAGS = -Wall -c -I${HOME}/local/include -I${HOME}/local/include/ncurses -g
LFLAGS = -L${HOME}/local/lib -L${HOME}/local/include/ncurses -L${HOME}/local/include -lncurses -lrt


all: tetra

tetra: main.o
	gcc main.o ${LFLAGS} -o tetra

main.o: main.c
	gcc ${CFLAGS} main.c

tags:
	ctags -R

clean:
	rm *.o
